import React, { Component } from "react";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";
import { connect } from "react-redux";
import { setAnswer } from "../../store/actions/answer";

class MutipleChoice extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: "",
    };
  }

  handleChange = (e) => {
    const { content, answers, id } = this.props.item;

    this.setState(
      {
        value: e.target.value,
      },
      () => {
        const answerId = this.state.value;
        const foughtAnswer = answers.find((item) => item.id === answerId);
        const cloneAnswersList = [...this.props.answersList];
        const foughtIndex = cloneAnswersList.findIndex((item) => {
          return item.questionId === id;
        });
        if (foughtIndex === -1) {
          const answerItem = {
            questionId: id,
            answer: { content: content, exact: foughtAnswer.exact },
          };
          cloneAnswersList.push(answerItem);
        } else {
          cloneAnswersList[foughtIndex].answer.exact = foughtAnswer.exact;
        }
        this.props.dispatch(setAnswer(cloneAnswersList));
      }
    );
  };

  render() {
    const { content, answers, id } = this.props.item;
    return (
      <div style={{ marginBottom: 20 }}>
        <FormControl component="fieldset">
          <FormLabel style={{ fontSize: 24, color: "#000" }} component="legend">
            Câu hỏi {id}:{content}
          </FormLabel>

          <RadioGroup onChange={this.handleChange}>
            {answers.map((answer) => (
              <FormControlLabel
                key={answer.id}
                value={answer.id}
                control={<Radio />}
                label={answer.content}
              />
            ))}
          </RadioGroup>
        </FormControl>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    answersList: state.answerList.answersList,
  };
};
export default connect(mapStateToProps)(MutipleChoice);
