import { Typography } from "@material-ui/core";
import React, { Component } from "react";
import TextField from "@material-ui/core/TextField";
import { connect } from "react-redux";
import { setAnswer } from "../../store/actions/answer";
class FillInBlank extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: "",
    };
    this.timer = null;
  }
  handleChange = (e) => {
    if (this.timer) {
      clearTimeout(this.timer);
    }
    this.timer = setTimeout(() => {
      const { id, content, answers } = this.props.item;
      this.setState({ value: e.target.value }, () => {
        const answerInput = this.state.value;
        const checkExact = () => {
          return answerInput.toLowerCase() === answers[0].content.toLowerCase();
        };
        const cloneAnswerList = [...this.props.answersList];
        const foughtIndex = cloneAnswerList.findIndex((item) => {
          return item.questionId === id;
        });
        if (foughtIndex === -1) {
          const answerItem = {
            questionId: id,
            answer: {
              content: content,
              exact: checkExact(),
            },
          };
          cloneAnswerList.push(answerItem);
        } else {
          cloneAnswerList[foughtIndex].answer.exact = checkExact();
        }
        this.props.dispatch(setAnswer(cloneAnswerList));
      });
    }, 400);
  };
  render() {
    const { id, content } = this.props.item;
    return (
      <div style={{ marginBottom: 20 }}>
        <form noValidate autoComplete="off">
          <Typography variant="h5">
            Câu hỏi {id}: {content}
          </Typography>
          <TextField
            onChange={this.handleChange}
            variant="outlined"
            fullWidth
            label="Nhập câu trả lời"
          />
        </form>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return { answersList: state.answerList.answersList };
};
export default connect(mapStateToProps)(FillInBlank);
