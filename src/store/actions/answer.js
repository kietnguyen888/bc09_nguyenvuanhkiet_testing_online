import { createAction } from ".";
import { actionType } from "./type";

export const setAnswer = (foughtAnswer) => {
  return (dispatch) => {
    dispatch(createAction(actionType.SET_ANSWER, foughtAnswer));
  };
};
