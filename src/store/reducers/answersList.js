import { actionType } from "../actions/type";

const initialState = {
  answersList: [],
};
const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionType.SET_ANSWER:
      state.answersList = action.payload;
      return { ...state };

    default:
      return state;
  }
};
export default reducer;
