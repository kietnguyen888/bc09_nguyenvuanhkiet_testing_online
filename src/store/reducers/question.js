import { actionType } from "../actions/type";

const initialState = {
  questionsList: [],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionType.SET_QUESTIONS:
      state.questionsList = action.payload;
      return { ...state };
    default:
      return state;
  }
};
export default reducer;
