import { Box, Button, Container, Typography } from "@material-ui/core";
import React, { Component } from "react";
import { connect } from "react-redux";
import { fetchQuestions } from "../../store/actions/question";
import MultipleChoice from "../../components/multipleChoice";
import FillInBlank from "../../components/fillInBlank";

class Home extends Component {
  checkResult = () => {
    let result = 0;
    for (const item of this.props.answersList) {
      if (item.answer.exact) {
        result++;
      }
    }
    alert(`Kết quả: ${result} / ${this.props.questionsList.length}`);
  };
  render() {
    return (
      <div>
        <Typography
          style={{ marginBottom: "20px" }}
          align="center"
          variant="h2"
          color="secondary"
        >
          Testing Online
        </Typography>
        <Container maxWidth="lg">
          {this.props.questionsList.map((item) => {
            return item.questionType === 1 ? (
              <MultipleChoice key={item.id} item={item} />
            ) : (
              <FillInBlank key={item.id} item={item} />
            );
          })}
        </Container>
        <Box align="center">
          <Button
            size="large"
            variant="contained"
            color="secondary"
            onClick={this.checkResult}
          >
            Submit
          </Button>
        </Box>
      </div>
    );
  }
  componentDidMount() {
    this.props.dispatch(fetchQuestions);
  }
}

const mapStateToProps = (state) => {
  return {
    questionsList: state.question.questionsList,
    answersList: state.answerList.answersList,
  };
};
export default connect(mapStateToProps)(Home);
